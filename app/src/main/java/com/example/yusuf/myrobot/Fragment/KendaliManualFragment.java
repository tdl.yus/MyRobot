package com.example.yusuf.myrobot.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
//import android.widget.Toast;

import com.example.yusuf.myrobot.R;
//import com.example.yusuf.myrobot.Utils.GetApi;
//import com.example.yusuf.myrobot.Utils.RetrofitBuilder;
import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;

public class KendaliManualFragment extends Fragment implements View.OnClickListener {
    ImageButton forward, back, left, right, stop;
//    GetApi getApi;
//    String apiKey = "3CONZQ1XSXKKJPZ4";
    ProgressDialog progressDialog;
    private Firebase mRef;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kendali_manual, container, false);
        Firebase.setAndroidContext(getActivity());
        getActivity().setTitle("My Robot");
        forward = view.findViewById(R.id.forward);
        forward.setOnClickListener(this);
        back = view.findViewById(R.id.back);
        back.setOnClickListener(this);
        left = view.findViewById(R.id.left);
        left.setOnClickListener(this);
        right = view.findViewById(R.id.right);
        right.setOnClickListener(this);
        stop = view.findViewById(R.id.stop);
        stop.setOnClickListener(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mRef = new Firebase("https://robotcontrol14.firebaseio.com");  // DataBase Profile Link
//        getApi = RetrofitBuilder.getBuild(getActivity());

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onClick(View v) {
    switch (v.getId()){
        case R.id.forward:
            progressDialog.setMessage("Send Data...");
            progressDialog.show();
            send(1, 0,0,0,0);
            break;
        case R.id.back:
            progressDialog.setMessage("Send Data...");
            progressDialog.show();
            send(0, 1,0,0,0);
            break;
        case R.id.left:
            progressDialog.setMessage("Send Data...");
            progressDialog.show();
            send(0, 0,0,1,0);
            break;
        case R.id.right:
            progressDialog.setMessage("Send Data...");
            progressDialog.show();
            send(0,0,1,0,0);
            break;
        case R.id.stop:
            progressDialog.setMessage("Send Data...");
            progressDialog.show();
            send(0,0,0,0,1);
            break;
        }
    }

//    private void post(String maju, String mundur, String kanan, String kiri, String stop) {
//        Call<Void> fieldCall;
//        fieldCall = getApi.Field(apiKey, maju, mundur, kanan, kiri, stop);
//        fieldCall.enqueue(new Callback<Void>() {
//            @Override
//            public void onResponse(Call<Void> call, Response<Void> response) {
//                Toast.makeText(getActivity(), String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
//                progressDialog.dismiss();
//            }
//
//            @Override
//            public void onFailure(Call<Void> call, Throwable t) {
//                Toast.makeText(getActivity(),"failure", Toast.LENGTH_SHORT).show();
//                progressDialog.dismiss();
//            }
//        });
//    }

    private void send(int maju, int mundur, int kanan, int kiri, int henti){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference forward = database.getReference("direction/maju");
        DatabaseReference backward = database.getReference("direction/mundur");
        DatabaseReference right = database.getReference("direction/kanan");
        DatabaseReference left = database.getReference("direction/kiri");
        DatabaseReference stop = database.getReference("direction/stop");

        forward.setValue(maju);
        backward.setValue(mundur);
        right.setValue(kanan);
        left.setValue(kiri);
        stop.setValue(henti);
        progressDialog.dismiss();
    }
}
