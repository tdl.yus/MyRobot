package com.example.yusuf.myrobot.Fragment.database;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.support.v4.app.Fragment;
import android.widget.ListView;

import com.example.yusuf.myrobot.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DatabaseFragment extends Fragment {
    private RecyclerView.Adapter adapter;
    private RecyclerView recyclerView;
    DatabaseReference database;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressDialog progressDialog;
    Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_database, container, false);
        getActivity().setTitle("Database");
        recyclerView = view.findViewById(R.id.RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setVerticalScrollBarEnabled(true);
        context = getActivity();
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Mengambil Data");

        new GetDataFromFirebase().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        database = FirebaseDatabase.getInstance().getReference("sensor");
        swipeRefreshLayout = view.findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getSensor();
            }
        });

        progressDialog.show();
        getSensor();
        return view;
    }

public void getSensor(){
    database.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()) {
                ArrayList<String> arrayList = new ArrayList<>();
                ArrayList<String> arrayid = new ArrayList<>();
                int jumlah = 0;
                int count = (int) dataSnapshot.getChildrenCount();
                for (DataSnapshot str : dataSnapshot.getChildren()) {
                    jumlah++;
                    if (jumlah > count - 20) {
                        arrayList.add(str.child("ppm").getValue().toString());
                        arrayid.add(str.getKey());
                    }
                }
                adapter = new RecyclerViewAdapter(arrayList, arrayid, getActivity());
                recyclerView.setAdapter(adapter);
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
            } else {
                Log.d("error", "Error trying to get classified ads for ");
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
            }
        }

        @Override
        public void onCancelled(DatabaseError error) {
            // Failed to read value
            progressDialog.dismiss();
            swipeRefreshLayout.setRefreshing(false);
            System.out.println("Failed to read value." + error.toException());
        }
    });
}

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private class GetDataFromFirebase extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }
}
