package com.example.yusuf.myrobot.Fragment.database;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.yusuf.myrobot.R;
import com.example.yusuf.myrobot.R.layout;
import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<String> values;
    private List<String> nomor;
    Context context;
    RecyclerViewAdapter(List<String> values,List<String> nomor, Context context ) {
        this.values = values;
        this.nomor = nomor;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout.item_database, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        holder.ppm.setText(values.get(position));
        holder.id.setText(nomor.get(position));
    }

    @Override
    public int getItemCount() {
       return values.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView ppm, id;
        public ViewHolder(View itemView) {
            super(itemView);
            ppm = itemView.findViewById(R.id.ppm);
            id = itemView.findViewById(R.id.id);
        }
    }
}
